<?php

namespace MB\TestBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Yaml\Dumper;

/**
 * Api controller
 *
 * @author Manuel Brossard <manuelbrossard2@gmail.com>
 * @Route("/api")
 */
class ApiController extends Controller
{
    /**
     * @Route
     * @Route("/{id}", requirements={"id"="\d+"})
     * @Method("GET")
     */
    public function indexAction($id = null)
    {
        $em = $this->getDoctrine()->getEntityManager();

        if (null !== $id) {
            $commands = $em->getRepository('MBTestBundle:Command')->find($id);
        } else {
            $commands = $em->getRepository('MBTestBundle:Command')->findAll();
        }

        $request  = $this->getRequest();
        $format = $request->query->get('format');

        return $this->formatResponse($commands, $format);
    }

    /**
     * Format Response according to $format parameter
     *
     * @param  array                 $commands Array of commands or command entity
     * @param  string                $format   Yaml or null (=json)
     * @return Response|JsonResponse           Formated Response
     */
    public function formatResponse($commands, $format = null)
    {
        if ($format == 'yaml') {
            $dumper = new Dumper(); 
            $yaml = $dumper->dump($commands, 0, 1, false, true);
            $response = new Response($yaml);
        } else {
            $response = new JsonResponse();
            $response->setData($commands);
        }

        return $response; 
    }
}