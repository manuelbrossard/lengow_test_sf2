<?php

namespace MB\TestBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use APY\DataGridBundle\Grid\Source\Entity;
use MB\TestBundle\Form\CommandType;
use MB\TestBundle\Entity\Command;
use Symfony\Component\HttpFoundation\Request;

/**
 * Default controller
 *
 * @author Manuel Brossard <manuelbrossard2@gmail.com>
 */
class DefaultController extends Controller
{
    /**
     * @Route
     * @Method("GET|POST")
     * @Template
     */
    public function indexAction(Request $request)
    {
        $translator = $this->get('translator');

        $command = new Command();
        $form = $this->createForm(new CommandType(), $command);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($command);
            $em->flush();
            
            $message = $translator->trans('forms.command.valid');
        } else {
            $message = $translator->trans('forms.command.error');
        }

        $source = new Entity('MBTestBundle:Command');
        $grid = $this->get('grid');
        $grid->setSource($source);

        return $grid->getGridResponse('MBTestBundle:Default:index.html.twig', array(
            'form' => $form->createView(),
            'grid' => $grid,
            'message' => isset($message) ? $message : null
        ));
    }
}