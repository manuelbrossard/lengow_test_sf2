<?php

namespace MB\TestBundle\XML;

use Psr\Log\LoggerInterface;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\Config\Util\XmlUtils;

/**
 * Parses an XML document from url
 *
 * @author Manuel Brossard <manuelbrossard2@gmail.com>
 */
class XMLParser
{
    /**
     * LoggerInterface Object
     *
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * TranslatorInterface Object
     *
     * @var TranslatorInterface
     */
    protected $translator;

    /**
     * Url to get XML from
     *
     * @var string
     */
    protected $urlOrders;

    public function __construct(LoggerInterface $logger, TranslatorInterface $translator, $urlOrders) 
    {
        $this->logger = $logger;
        $this->translator = $translator;
        $this->urlOrders = $urlOrders;
    }

    /**
     * Returns the XML document formatted into a PHP array
     *
     * @return array The formatted document
     */
    public function XMLToArray()
    {
        $this->logUsage();

        $xml = XmlUtils::loadFile($this->urlOrders);
        $array = $this->loadFromExtensions($xml);

        return $array;
    }

    /**
     * Converts an XML file to a PHP array
     *
     * @param \DOMDocument $xml XML file
     * @return array            PHP array
     */
    public function loadFromExtensions(\DOMDocument $xml)
    {
        foreach ($xml->documentElement->childNodes as $node) {
            if (!$node instanceof \DOMElement) {
                continue;
            }

            $values = XmlUtils::convertDomElementToArray($node);
            if (!is_array($values)) {
                $values = array();
            }
        }

        return $values;
    }

    /**
     * Logs the call to this class
     *
     */
    public function logUsage()
    {
        $message = $this->translator->trans('logger.url_orders');
        $this->logger->info($message);
    }
}