<?php
namespace MB\TestBundle\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use MB\TestBundle\Entity\Command;

class SaveCommandsCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('mb:saveCommands')
            ->setDescription('Save commands from distant XML file');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();
        $translator = $container->get('translator');
        $em = $container->get('doctrine')->getEntityManager();

        $xmlParser = $container->get('lengow_test');
        $array = $xmlParser->XMLToArray();
        $commands = $array['order'];
        
        $i = 0;
        foreach ($commands as $command) {
            $entity = $em->getRepository('MBTestBundle:Command')->findOneByOrderId($command['order_id']);

            if (null === $entity) {
                $date = new \DateTime($command['order_purchase_date'].' '.$command['order_purchase_heure']);

                $entity = new Command();
                $entity->setOrderId($command['order_id']);
                $entity->setMarketplace($command['marketplace']);
                $entity->setOrderStatus($command['order_status']['lengow']);
                $entity->setOrderPurchaseDate($date);
                $entity->setOrderAmount($command['order_amount']);

                $em->persist($entity);
                $i++;
            } else {
                $em->detach($entity);
            }
        }
        $em->flush();

        if ($i) {
            $message = $translator->transChoice('commands.save_commands.created', $i, array('%count%' => $i));
        } else {
            $message = $translator->trans('commands.save_commands.none');
        }

        $output->writeln($message);
    }
}