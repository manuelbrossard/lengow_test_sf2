<?php

namespace MB\TestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use APY\DataGridBundle\Grid\Mapping as GRID;

/**
 * Command
 *
 * @ORM\Table()
 * @ORM\Entity
 * @GRID\Source(columns="id, orderId, marketplace, orderStatus, orderPurchaseDate, orderAmount")
 */
class Command
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;

    /**
     * @var string
     *
     * @ORM\Column(name="order_id", type="string", length=255)
     */
    public $orderId;

    /**
     * @var string
     *
     * @ORM\Column(name="marketplace", type="string", length=255)
     */
    public $marketplace;

    /**
     * @var string
     *
     * @ORM\Column(name="order_status", type="string", length=255, nullable=true)
     */
    public $orderStatus;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="order_purchase_date", type="datetime")
     */
    public $orderPurchaseDate;

    /**
     * @var float
     *
     * @ORM\Column(name="order_amount", type="float")
     */
    public $orderAmount;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set orderId
     *
     * @param string $orderId
     * @return Command
     */
    public function setOrderId($orderId)
    {
        $this->orderId = $orderId;

        return $this;
    }

    /**
     * Get orderId
     *
     * @return string 
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * Set marketplace
     *
     * @param string $marketplace
     * @return Command
     */
    public function setMarketplace($marketplace)
    {
        $this->marketplace = $marketplace;

        return $this;
    }

    /**
     * Get marketplace
     *
     * @return string 
     */
    public function getMarketplace()
    {
        return $this->marketplace;
    }

    /**
     * Set orderStatus
     *
     * @param string $orderStatus
     * @return Command
     */
    public function setOrderStatus($orderStatus)
    {
        $this->orderStatus = $orderStatus;

        return $this;
    }

    /**
     * Get orderStatus
     *
     * @return string 
     */
    public function getOrderStatus()
    {
        return $this->orderStatus;
    }

    /**
     * Set orderPurchaseDate
     *
     * @param \DateTime $orderPurchaseDate
     * @return Command
     */
    public function setOrderPurchaseDate(\DateTime $orderPurchaseDate)
    {
        $this->orderPurchaseDate = $orderPurchaseDate;

        return $this;
    }

    /**
     * Get orderPurchaseDate
     *
     * @return \DateTime 
     */
    public function getOrderPurchaseDate()
    {
        return $this->orderPurchaseDate;
    }

    /**
     * Set orderAmount
     *
     * @param float $orderAmount
     * @return Command
     */
    public function setOrderAmount($orderAmount)
    {
        $this->orderAmount = str_replace(',', '.', $orderAmount);

        return $this;
    }

    /**
     * Get orderAmount
     *
     * @return float 
     */
    public function getOrderAmount()
    {
        return $this->orderAmount;
    }
}